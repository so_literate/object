// Package object is an implementation of the JSON object with ordered elements.
package object

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
)

// ErrInvalidJSON returns in unmarshal method when decoder got unexpected JSON.
var ErrInvalidJSON = errors.New("invalid json")

// UnmarshalJSON decodes JSON to the dst type of the object value.
// It takes reproducer of the new value destination and setter function to write a decoded value with key.
func UnmarshalJSON(data []byte, reproducer func() (dst interface{}), setter func(key string, value interface{})) error {
	dec := json.NewDecoder(bytes.NewReader(data))

	t, err := dec.Token()
	if err != nil {
		return fmt.Errorf("failed to get first token: %w", err)
	}

	if delim, ok := t.(json.Delim); !ok || delim != '{' {
		return fmt.Errorf("%w: expect JSON object open with '{', got %T (%v)", ErrInvalidJSON, t, t)
	}

	for dec.More() {
		t, err = dec.Token()
		if err != nil {
			return fmt.Errorf("failed to get the next token: %w", err)
		}

		key, ok := t.(string)
		if !ok {
			return fmt.Errorf("%w: expected 'key' string token, got %T (%v)", ErrInvalidJSON, t, t)
		}

		dst := reproducer()

		if err = dec.Decode(dst); err != nil {
			return fmt.Errorf("failed to decode value: %w", err)
		}

		setter(key, dst)
	}

	t, err = dec.Token()
	if err != nil {
		return fmt.Errorf("failed to get the last token: %w", err)
	}

	if delim, ok := t.(json.Delim); !ok || delim != '}' {
		return fmt.Errorf("%w: expect JSON object close with '}', got %T (%v)", ErrInvalidJSON, t, t)
	}

	return nil
}

// MarshalJSON encodes every element of the Object to JSON.
// It takes number of the keys in the object and getter function to get the key and its value by index.
func MarshalJSON(keysNumber int, getter func(i int) (key string, value interface{})) ([]byte, error) {
	buf := bytes.Buffer{}
	enc := json.NewEncoder(&buf)

	buf.WriteByte('{')

	for i := 0; i < keysNumber; i++ {
		key, value := getter(i)

		if i != 0 {
			buf.WriteByte(',')
		}

		enc.Encode(key) // nolint:errcheck,gosec,errchkjson // Can't return error here because "key" is a string.

		buf.WriteByte(':')

		if err := enc.Encode(value); err != nil {
			return nil, fmt.Errorf("bad value (%T) with key %q: %w", value, key, err)
		}
	}

	buf.WriteByte('}')

	return buf.Bytes(), nil
}
