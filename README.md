Object
========

This is an experimental implementation of the JSON object with ordered elements. 

TODO: Use generics to simplify methods and reduce user code.

Can to parse this 
```json
{
	"key": {"field": 1},
	"key2": {"field": 2}
}
```

To a slice of types with key and value.

```go
type Value struct {
	Field int
}

type Elem struct {
	Key   string
	Value *Value
}

type Object []Elem // <- Object with key and value.
```