package object_test

import (
	"encoding/json"
	"fmt"

	"gitlab.com/so_literate/object"
)

type ObjectValue struct {
	Field string `json:"field"`
	Slice []int  `json:"slice"`
}

type KeyValue struct {
	Key   string
	Value *ObjectValue
}

type Object []KeyValue

func (o Object) MarshalJSON() ([]byte, error) {
	return object.MarshalJSON(len(o), func(i int) (string, interface{}) {
		elem := o[i]
		return elem.Key, elem.Value
	})
}

func (o *Object) UnmarshalJSON(data []byte) error {
	res := make(Object, 0)

	defer func() { *o = res }()

	return object.UnmarshalJSON(
		data,
		func() interface{} {
			return new(ObjectValue)
		},
		func(key string, value interface{}) {
			res = append(res, KeyValue{
				Key:   key,
				Value: value.(*ObjectValue),
			})
		},
	)
}

func Example() {
	type root struct {
		Field  string `json:"field"`
		Object Object `json:"object"`
	}

	data := `{"field":"root field","object":{"key1":{"field":"fk1","slice":[1]},"key_two":{"field":"fk2","slice":[2]}}}`
	value := root{}

	err := json.Unmarshal([]byte(data), &value)
	if err != nil {
		fmt.Println("json.Unmarshal:", err)
		return
	}

	fmt.Println("Unmarshal:")
	fmt.Println(value.Field)
	for _, elem := range value.Object {
		fmt.Println(elem.Key, elem.Value.Field, elem.Value.Slice)
	}

	fmt.Println("\nMarshal:")

	gotData, err := json.Marshal(value)
	if err != nil {
		fmt.Println("json.Marshal:", err)
		return
	}

	fmt.Println(string(gotData))

	// Output:
	//
	// Unmarshal:
	// root field
	// key1 fk1 [1]
	// key_two fk2 [2]
	//
	// Marshal:
	// {"field":"root field","object":{"key1":{"field":"fk1","slice":[1]},"key_two":{"field":"fk2","slice":[2]}}}
}
